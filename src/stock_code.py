import requests
import pickle
import os
from log import Log
from bs4 import BeautifulSoup

logger = Log("stock_eps.log", {"level": Log.ERROR}).get_logger()


class StockCode(object):
    def __init__(self):
        self.base_url = "https://isin.twse.com.tw/isin/C_public.jsp?strMode=2"
        self.pickle_file = "./stock_id.pickle"
        self.stock_map = {}

    def get_stock_name(self, stock_id):
        self.load_pickle()
        return self.stock_map[str(stock_id)]

    def get_stock_id(self, target_stock_name):
        self.load_pickle()
        for stock_id, stock_name in self.stock_map.items():
            if stock_name == target_stock_name:
                return stock_id

    def load_pickle(self):
        if os.path.isfile(self.pickle_file) and not self.stock_map:
            with open(self.pickle_file, "rb") as pickle_file:
                self.stock_map = pickle.load(pickle_file)

    def fetch(self):
        if os.path.isfile(self.pickle_file):
            return True

        try:
            resp = requests.get(self.base_url)
        except requests.exceptions.ConnectionError:
            logger.error("無法連線，請檢查網路")
        else:
            soup = BeautifulSoup(resp.text, "lxml")
            table = soup.select_one("table[class=h4]")

            for clo in table.select("tr"):
                stock_id_info = clo.select_one("td").text.split("　")
                if len(stock_id_info) == 2:
                    self.stock_map[stock_id_info[0]] = stock_id_info[1]
            logger.debug("total stock id: %s", len(self.stock_map))

            with open(self.pickle_file, "wb") as pickle_file:
                pickle.dump(self.stock_map, pickle_file)
