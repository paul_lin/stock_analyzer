import logging


class Log(object):
    DEBUG = logging.DEBUG
    INFO = logging.INFO
    WARNING = logging.WARNING
    ERROR = logging.ERROR
    CRITICAL = logging.CRITICAL

    def __init__(self, name, config: dict):
        self.logger_name = name
        self.logger = logging.getLogger(name)
        self.logger.setLevel(config["level"])
        self.init_config(config)

    def init_config(self, config):
        formatter = config.get("formatter")
        if not formatter:
            formatter = logging.Formatter("[%(levelname)s %(asctime)s %(name)s %(module)s:%(lineno)d] %(message)s")

        file_handler = logging.FileHandler(self.logger_name)
        file_handler.setFormatter(formatter)
        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)

        self.logger.addHandler(file_handler)
        self.logger.addHandler(console_handler)

    def get_logger(self):
        return self.logger
