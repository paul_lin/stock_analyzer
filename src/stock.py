import requests
import csv
import argparse
from log import Log
from bs4 import BeautifulSoup
from stock_code import StockCode

logger = Log("stock_eps.log", {"level": Log.ERROR}).get_logger()


class StockCrawler(object):
    def __init__(self, path):
        self.base_url = 'https://histock.tw/stock/{}/%E6%AF%8F%E8%82%A1%E7%9B%88%E9%A4%98'
        self.path = path

    def write_to_csv(self, csv_content):
        file_path = self.path
        if not self.path:
            file_path = "output.csv"

        with open(file_path, "a", newline='') as csv_file:
            writer = csv.writer(csv_file)
            writer.writerows(csv_content)

    def get_table(self, soup):
        eps_table = soup.select_one("table[class~=tb-stock]")
        output_rows = []

        # 解欄位名稱
        columns_name = eps_table.select("tr:nth-of-type(1) th")
        all_column_name = []
        for column_name in columns_name[1:]:
            all_column_name.append(column_name.text)
        output_rows.append(all_column_name)

        # 解每欄
        for row in eps_table.select("tr"):
            columns = row.select("td")
            output_row = []
            for column in columns:
                output_row.append(column.text)
            output_rows.append(output_row)
        return output_rows

    def insert_new_line(self, number_of_line, table):
        for i in range(number_of_line):
            table.insert(0, [])
        return table

    def get_stock_info(self, stock):
        stock_code = StockCode()
        stock_code.fetch()
        stock_full_name = []
        try:
            stock_name = stock_code.get_stock_name(int(stock))
            if stock_name:
                stock_full_name = [stock, stock_name]
        except ValueError:
            stock_id = stock_code.get_stock_id(stock)
            if stock_id:
                stock_full_name = [stock_id, stock]
        return stock_full_name

    def start(self, stock):
        stock_full_name = self.get_stock_info(stock)
        logger.debug("stock_full_name: %s", stock_full_name)
        table = []

        if stock_full_name:
            try:
                resp = requests.get(self.base_url.format(stock_full_name[0]))
            except requests.exceptions.ConnectionError:
                logger.error("無法連線，請檢查網路")
            else:
                soup = BeautifulSoup(resp.text, "lxml")
                table = self.get_table(soup)
                logger.debug("stock eps table: %s", table)
                table.insert(0, [stock_full_name[0] + " " + stock_full_name[1]])
                self.insert_new_line(2, table)
        else:
            logger.error("錯誤的股票名稱或代碼: %s", stock)
        return table


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--stock", nargs="+", help="Get stock EPS.", required=True)
    parser.add_argument("-o", "--output", help="The output path (include file name).")

    args = parser.parse_args()
    if args.stock:
        stock_crawler = StockCrawler(args.output)
        for stock in args.stock:
            table = stock_crawler.start(stock)
            if table:
                stock_crawler.write_to_csv(table)
            else:
                logger.error("找不到該股票的 EPS 資訊")
